#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TOOLCHAIN=""
TOOLCHAIN64="https://dl.espressif.com/dl/xtensa-lx106-elf-linux64-1.22.0-92-g8facf4c-5.2.0.tar.gz"
TOOLCHAIN32="https://dl.espressif.com/dl/xtensa-lx106-elf-linux32-1.22.0-92-g8facf4c-5.2.0.tar.gz"
ARCH=""
DIR_TOOLCHAIN_ESP32="xtensa-lx106-elf"
DIR_TOOLCHAIN_ESP32_EXPORT="export PATH=\"\$PATH:$DIR/xtensa-lx106-elf/bin\""
DIR_ESP_IDF_EXPORT="export IDF_PATH=$DIR/ESP8266_RTOS_SDK"

# Check machine is 32 or 64bit
ARCH="$(uname -m)"
if [ "$ARCH" == "x86_64" ]; then
    TOOLCHAIN=$TOOLCHAIN64
else
    TOOLCHAIN=$TOOLCHAIN32
fi
echo $TOOLCHAIN

# Install required package
sudo apt-get install gcc git wget make libncurses-dev flex bison gperf python python-serial

# Setup toolchain
cd $DIR
wget -qO- $TOOLCHAIN | tar xvz -C "$DIR"
echo $DIR_TOOLCHAIN_ESP32_EXPORT >> ~/.profile
echo $DIR_ESP_IDF_EXPORT >> ~/.profile
source ~/.profile

