#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TOOLCHAIN="https://dl.espressif.com/dl/xtensa-lx106-elf-win32-1.22.0-92-g8facf4c-5.2.0.tar.gz"
ARCH=""
DIR_TOOLCHAIN_ESP32="xtensa-esp32-elf"
DIR_TOOLCHAIN_ESP32_EXPORT="export PATH=\"\$PATH:$DIR/xtensa-lx106-elf/bin\""
DIR_ESP_IDF_EXPORT="export IDF_PATH=$DIR/ESP8266_RTOS_SDK"

# Setup toolchain
cd $DIR
wget -qO- $TOOLCHAIN | tar xvz -C "$DIR"
echo $DIR_ESP_IDF_EXPORT >> ~/.profile
echo $DIR_TOOLCHAIN_ESP32_EXPORT >> ~/.profile
source ~/.profile
