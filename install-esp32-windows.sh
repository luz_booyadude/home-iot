#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TOOLCHAIN=""
TOOLCHAIN64="https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz"
TOOLCHAIN32="https://dl.espressif.com/dl/xtensa-esp32-elf-linux32-1.22.0-80-g6c4433a-5.2.0.tar.gz"
ARCH=""
DIR_TOOLCHAIN_ESP32="xtensa-esp32-elf"
DIR_TOOLCHAIN_ESP32_EXPORT="export PATH=\"\$PATH:$DIR/xtensa-esp32-elf/bin\""
DIR_ESP_IDF_EXPORT="export IDF_PATH=$DIR/esp-idf"

# Setup toolchain
cd $DIR
echo $DIR_ESP_IDF_EXPORT >> ~/.profile
source ~/.profile

