#ifndef CFG_PROJECT_H
#define CFG_PROJECT_H

#ifdef __cplusplus
extern "C" {
#endif

/* Project Selection. Uncomment one of the below list */
#define BOARD_NODEMCU_1_0     
/* Project Selection. Uncomment one of the above list */

#ifdef BOARD_NODEMCU_1_0

typedef enum
{
    GPIO_D0 = 16U,
    GPIO_D1 = 5,
    GPIO_D2 = 4,
    GPIO_D3 = 0,
    GPIO_D4 = 2,
    GPIO_D5 = 14,
    GPIO_D6 = 12,
    GPIO_D7 = 13,
    GPIO_D8 = 15,
    GPIO_RX = 3,
    GPIO_TX = 1,
    GPIO_SD2 = 9,
    GPIO_SD3 = 10
}gpio_t;

#endif


#ifdef __cplusplus
}
#endif

#endif // CFG_PROJECT_H