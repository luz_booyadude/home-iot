#ifndef WIFI_IF_H
#define WIFI_IF_H

#ifdef __cplusplus
extern "C" {
#endif

void wifiScanTask( void * parameter );

#ifdef __cplusplus
}
#endif

#endif // WIFI_IF_H