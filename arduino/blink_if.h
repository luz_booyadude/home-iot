#ifndef BLINK_IF_H
#define BLINK_IF_H

#ifdef __cplusplus
extern "C" {
#endif

void taskOne( void * parameter );

#ifdef __cplusplus
}
#endif

#endif // BLINK_IF_H