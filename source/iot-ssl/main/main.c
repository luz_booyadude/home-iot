/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "blink.h"
#include "gpio/gpio_if.h"
#include "esp_system.h"

#include "nvs_flash.h"
#include "esp_log.h"
#include "wifi/wifi_drv.h"
#include "comms/comms.h"
#include "mqtt_drv/mqtt_drv.h"
#include "debug/debug_if.h"

/* Can run 'make menuconfig' to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/

#define PIR_GPIO GPIO_D1
#define LED_GPIO GPIO_D0

void pirSensorTask(void *parameter)
{
    uint32_t count = 0U;
    uint32_t tick = 0U;
    bool switchOn = false;
    /* Set the GPIO as a push/pull output */
    // gpio_set_direction(PIR_GPIO, GPIO_MODE_INPUT);
    // gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);
    gpio_pinMode(PIR_GPIO, GPIO_INPUT);
    gpio_pinMode(LED_GPIO, GPIO_OUTPUT);
    while (1)
    {
        if (gpio_readPin(PIR_GPIO))
        {
            tick = xTaskGetTickCount();
            printf("Object detected %d %d!\n", count++, tick);
            switchOn = true;
        }
        if ((switchOn == true) && ((xTaskGetTickCount() - tick) > (10000 / portTICK_PERIOD_MS)))
        {
            switchOn = false;
        }
        if (switchOn)
        {
            gpio_writePin(LED_GPIO, GPIO_HIGH);
        }
        else
        {
            gpio_writePin(LED_GPIO, GPIO_LOW);
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void app_main()
{
    rtc_clk_cpu_freq_set(RTC_CPU_FREQ_160M);

    // Initialize NVS
    esp_err_t ret = nvs_flash_init();

    if (ret == ESP_ERR_NVS_NO_FREE_PAGES)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    debugSystemInfo();  // show system info

    wifi_drvInit();
    comms_init();
    mqtt_drvInit();

    xTaskCreate(&debugInfoTask, "debuginfo", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);
    xTaskCreate(&blinkTask, "blinkTask", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);
    // xTaskCreate(&pirSensorTask, "PIR Task", configMINIMAL_STACK_SIZE, NULL, 5, NULL);
}
