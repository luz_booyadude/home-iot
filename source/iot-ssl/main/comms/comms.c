#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "comms.h"
#include "esp_log.h"

static const char *TAG = "COMMS";

static void comms_task(void *param);
static void comms_taskMsgHandle(comms_taskInfo_t *taskInfo, comms_msgHdr_t *msgHdr);
static void comms_freeMsg(comms_msgHdr_t *msgHdr);

// static QueueHandle_t comms_queueHandle;
// static TaskHandle_t comms_taskHandle;
static comms_taskInfo_t comms_taskInfo;
static uint8_t subscriberCount = 0;

static comms_subscriberList_t comms_subscriberList[NUM_OF_SUBSCRIBER];

void comms_init(void)
{
    BaseType_t taskReturned = pdFAIL;

    comms_taskInfo.commsQueue = xQueueCreate(COMMS_MAX_QUEUE, (sizeof(comms_msgHdr_t) * NUM_OF_SUBSCRIBER));
    if(comms_taskInfo.commsQueue == NULL)
    {
        ESP_LOGE(TAG, "Fail to create queue!");
    }

    taskReturned = xTaskCreate(&comms_task, "comms_task", 2048, NULL, tskIDLE_PRIORITY, &(comms_taskInfo.taskHandle));
    if(taskReturned != pdPASS)
    {
        ESP_LOGE(TAG, "Fail to create task!");
    }

    memset(&comms_subscriberList, 0, (sizeof(comms_subscriberList_t) * NUM_OF_SUBSCRIBER));
}

int comms_registerSubscriber(comms_subscriber_t subscriber, comms_msgHandler_t comms_msgHandler)
{
    int result = -1;

    if(subscriberCount < NUM_OF_SUBSCRIBER)
    {
        if((subscriber < NUM_OF_SUBSCRIBER) && (comms_msgHandler != NULL))
        {
            comms_subscriberList[subscriberCount].subscriber = subscriber;
            comms_subscriberList[subscriberCount].msgHandler = comms_msgHandler;
            subscriberCount++;
            result = 0;
        }
        else
        {
            ESP_LOGE(TAG, "Fail to register subscriber!");
        }
    }
    return result;
}

int comms_sendMsg(comms_msgHdr_t *msg)
{
    BaseType_t result = pdFAIL;
    result = xQueueSend(comms_taskInfo.commsQueue, msg, 0);
    if(result)
    {
        free(msg);
    }
    return result;
}

static void comms_task(void *param)
{
    comms_msgHdr_t msg;

    while(1)
    {
        if(xQueueReceive( comms_taskInfo.commsQueue, &msg, 0 ))
        {
            comms_taskMsgHandle(&comms_taskInfo, &msg);

            comms_freeMsg(&msg);
        }
        // watchdog_kick();
    }
}

static void comms_taskMsgHandle(comms_taskInfo_t *taskInfo, comms_msgHdr_t *msgHdr)
{
    for(uint8_t i = 0; i < NUM_OF_SUBSCRIBER; i++)
    {
        if((msgHdr->msgData.dataPtr != NULL) && (comms_subscriberList[i].msgHandler != NULL))
        {
            if(comms_subscriberList[i].subscriber == (comms_subscriber_t)msgHdr->source)
            {
                comms_subscriberList[i].msgHandler(msgHdr);
                free(msgHdr->msgData.dataPtr);  // release memory upon successful send
            }
        }
    }
}

static void comms_freeMsg(comms_msgHdr_t *msgHdr)
{
    if(msgHdr != NULL)
    {
        if(msgHdr->msgData.dataPtr != NULL)
        {
            free(msgHdr->msgData.dataPtr);
        }
        // free(msgHdr);
    }
}