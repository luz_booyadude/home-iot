#ifndef COMMS_H
#define COMMS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#define COMMS_MAX_QUEUE         5

typedef enum
{
    SUBSYSTEM_WIFI = 0,
    SUBSYSTEM_BLINK,
    NUM_OF_SUBSCRIBER
}comms_subscriber_t;

typedef union
{
    void* dataPtr;
    uint8_t dataU8[4];
    int8_t  dataI8[4];
    uint16_t dataU16[2];
    int16_t  dataI16[2];
    uint32_t dataU32;
    int32_t  dataI32;
        
}comms_shortMsgData_t;

typedef struct
{
    uint8_t dest;
    uint8_t source;

    uint16_t msgSize;
    uint16_t msgType;

    comms_shortMsgData_t msgData;
}comms_msgHdr_t;

typedef struct
{
    QueueHandle_t commsQueue;
    TaskHandle_t  taskHandle;
}comms_taskInfo_t;

typedef void (*comms_msgHandler_t)(comms_msgHdr_t *msg);

typedef struct
{
    comms_subscriber_t subscriber;
    comms_msgHandler_t msgHandler;
}comms_subscriberList_t;

void comms_init(void);
int comms_registerSubscriber(comms_subscriber_t subscriber, comms_msgHandler_t comms_msgHandler);
int comms_sendMsg(comms_msgHdr_t *msg);

#ifdef __cplusplus
}
#endif

#endif // COMMS_H