#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "gpio/gpio_if.h"
#include "blink.h"

#include "comms/comms.h"
#include "esp_log.h"

#define BLINK_GPIO GPIO_D2

static void blink_commsHandler(comms_msgHdr_t *msg);

const char *TAG = "blink";

void blinkTask( void * parameter )
{
    uint32_t count = 0;
    uint32_t ticks = 0;
    uint8_t pinState = 0;
    comms_registerSubscriber(SUBSYSTEM_BLINK, blink_commsHandler);
    // gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_pinMode(BLINK_GPIO, 1);
    while(1) 
    {
        /* Blink off (output low) */
        if((xTaskGetTickCount() - ticks) >= pdMS_TO_TICKS(1000))
        {
            gpio_writePin(BLINK_GPIO, pinState);
            pinState ^= 1U;  // toggle bit
            // Test comms
            comms_msgHdr_t *msg;
            uint32_t *counter;
            msg = malloc(sizeof(comms_msgHdr_t));
            counter = malloc(sizeof(uint32_t));
            if((msg != NULL) && (counter != NULL))
            {
                msg->dest = SUBSYSTEM_BLINK;
                msg->source = SUBSYSTEM_BLINK;
                msg->msgSize = sizeof(uint32_t);
                *counter = count;
                msg->msgData.dataPtr = counter;

                if(!comms_sendMsg(msg))
                {
                    free(counter);  // release counter if send fail.
                }
            }
            count++;

            ticks = xTaskGetTickCount();
        }
    }
}

static void blink_commsHandler(comms_msgHdr_t *msg)
{
    uint32_t *counter = msg->msgData.dataPtr;
    ESP_LOGI(TAG, "Counter is %d", *counter);
}