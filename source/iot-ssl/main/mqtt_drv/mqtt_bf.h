/**
 * This header will handle the command request from MQTT
 */

#ifndef MQTT_BF_H
#define MQTT_BF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

void mqtt_bfInit(void);

#ifdef __cplusplus
}
#endif

#endif // MQTT_BF_H