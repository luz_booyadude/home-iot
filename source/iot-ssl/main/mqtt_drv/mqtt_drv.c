#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#ifdef CONFIG_EXAMPLE_FILESYSTEM_CERTS
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"
#endif

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"

#include "mqtt_drv.h"
#include "../wifi/wifi_drv.h"
#include "mqtt_bf.h"


const char *MQTT_DRV_TAG = "mqtt_drv";  // tag for log output
static AWS_IoT_Client client;  // mqtt client handler

#if defined(CONFIG_EXAMPLE_EMBEDDED_CERTS)

extern const uint8_t aws_root_ca_pem_start[] asm("_binary_aws_root_ca_pem_start");
extern const uint8_t aws_root_ca_pem_end[] asm("_binary_aws_root_ca_pem_end");
extern const uint8_t certificate_pem_crt_start[] asm("_binary_certificate_pem_crt_start");
extern const uint8_t certificate_pem_crt_end[] asm("_binary_certificate_pem_crt_end");
extern const uint8_t private_pem_key_start[] asm("_binary_private_pem_key_start");
extern const uint8_t private_pem_key_end[] asm("_binary_private_pem_key_end");

#elif defined(CONFIG_EXAMPLE_FILESYSTEM_CERTS)

static const char * DEVICE_CERTIFICATE_PATH = CONFIG_EXAMPLE_CERTIFICATE_PATH;
static const char * DEVICE_PRIVATE_KEY_PATH = CONFIG_EXAMPLE_PRIVATE_KEY_PATH;
static const char * ROOT_CA_PATH = CONFIG_EXAMPLE_ROOT_CA_PATH;

#else
#error "Invalid method for loading certs"
#endif

// /**
//  * @brief Default MQTT HOST URL is pulled from the aws_iot_config.h
//  */
// char mqtt_drvHost[255] = MQTT_BROKER;

// /**
//  * @brief Default MQTT port is pulled from the aws_iot_config.h
//  */
// uint32_t mqtt_drvPort = MQTT_PORT;

static void iot_subscribe_callback_handler(AWS_IoT_Client *pClient,
                                           char *topicName,
                                           uint16_t topicNameLen,
                                           IoT_Publish_Message_Params *params,
                                           void *pData)
{
    ESP_LOGI(MQTT_DRV_TAG, "Subscribe callback");
    ESP_LOGI(MQTT_DRV_TAG, "%.*s\t%.*s", topicNameLen, topicName, (int) params->payloadLen, (char *)params->payload);
}

static void disconnectCallbackHandler(AWS_IoT_Client *pClient, void *data)
{
    ESP_LOGW(MQTT_DRV_TAG, "MQTT Disconnect");
    IoT_Error_t rc = FAILURE;

    if(NULL == pClient)
    {
        return;
    }

    if(aws_iot_is_autoreconnect_enabled(pClient))
    {
        ESP_LOGI(MQTT_DRV_TAG, "Auto Reconnect is enabled, Reconnecting attempt will start now");
    } 
    else
    {
        ESP_LOGW(MQTT_DRV_TAG, "Auto Reconnect not enabled. Starting manual reconnect...");
        rc = aws_iot_mqtt_attempt_reconnect(pClient);
        if(NETWORK_RECONNECTED == rc)
        {
            ESP_LOGW(MQTT_DRV_TAG, "Manual Reconnect Successful");
        }
        else
        {
            ESP_LOGW(MQTT_DRV_TAG, "Manual Reconnect Failed - %d", rc);
        }
    }
}

static void mqtt_drvTask(void *param)
{
    IoT_Error_t rc = FAILURE;

    IoT_Client_Init_Params mqttInitParams = iotClientInitParamsDefault;
    IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;

    ESP_LOGI(MQTT_DRV_TAG, "AWS IoT SDK Version %d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

    mqttInitParams.enableAutoReconnect = false; // We enable this later below
    mqttInitParams.pHostURL = MQTT_BROKER;
    mqttInitParams.port = MQTT_PORT;

#if defined(CONFIG_EXAMPLE_EMBEDDED_CERTS)
    mqttInitParams.pRootCALocation = (const char *)aws_root_ca_pem_start;
    mqttInitParams.pDeviceCertLocation = (const char *)certificate_pem_crt_start;
    mqttInitParams.pDevicePrivateKeyLocation = (const char *)private_pem_key_start;

#elif defined(CONFIG_EXAMPLE_FILESYSTEM_CERTS)
    mqttInitParams.pRootCALocation = ROOT_CA_PATH;
    mqttInitParams.pDeviceCertLocation = DEVICE_CERTIFICATE_PATH;
    mqttInitParams.pDevicePrivateKeyLocation = DEVICE_PRIVATE_KEY_PATH;
#endif

    mqttInitParams.mqttCommandTimeout_ms = 20000;
    mqttInitParams.tlsHandshakeTimeout_ms = 5000;
    mqttInitParams.isSSLHostnameVerify = true;
    mqttInitParams.disconnectHandler = disconnectCallbackHandler;
    mqttInitParams.disconnectHandlerData = NULL;

#ifdef CONFIG_EXAMPLE_SDCARD_CERTS
    ESP_LOGI(MQTT_DRV_TAG, "Mounting SD card...");
    sdmmc_host_t host = SDMMC_HOST_DEFAULT();
    sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();
    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
        .format_if_mount_failed = false,
        .max_files = 3,
    };
    sdmmc_card_t* card;
    esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);
    if (ret != ESP_OK) {
        ESP_LOGE(MQTT_DRV_TAG, "Failed to mount SD card VFAT filesystem. Error: %s", esp_err_to_name(ret));
        abort();
    }
#endif

    rc = aws_iot_mqtt_init(&client, &mqttInitParams);
    if(SUCCESS != rc) {
        ESP_LOGE(MQTT_DRV_TAG, "aws_iot_mqtt_init returned error : %d ", rc);
        abort();
    }
    ESP_LOGI(MQTT_DRV_TAG, "Successfully init MQTT");

    /* Wait for WiFI to show as connected */
    ESP_LOGI(MQTT_DRV_TAG, "Waiting for Wifi to be connected...");
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
                        false, true, portMAX_DELAY);

    connectParams.keepAliveIntervalInSec = 10;
    connectParams.isCleanSession = true;
    connectParams.MQTTVersion = MQTT_3_1_1;
    /* Client ID is set in the menuconfig of the example */
    connectParams.pClientID = CONFIG_AWS_EXAMPLE_CLIENT_ID;
    connectParams.clientIDLen = (uint16_t) strlen(CONFIG_AWS_EXAMPLE_CLIENT_ID);
    connectParams.isWillMsgPresent = false;

    ESP_LOGI(MQTT_DRV_TAG, "Connecting to %s:%d...", MQTT_BROKER, MQTT_PORT);
    do {
        rc = aws_iot_mqtt_connect(&client, &connectParams);
        if(SUCCESS != rc) {
            ESP_LOGE(MQTT_DRV_TAG, "Error(%d) connecting to %s:%d", rc, mqttInitParams.pHostURL, mqttInitParams.port);
            vTaskDelay(1000 / portTICK_RATE_MS);
        }
    } while(SUCCESS != rc);

    /*
     * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
     *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
     *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
     */
    rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);
    if(SUCCESS != rc) {
        ESP_LOGE(MQTT_DRV_TAG, "Unable to set Auto Reconnect to true - %d", rc);
        abort();
    }

    const char *TOPIC = "test_topic/esp32";
    const int TOPIC_LEN = strlen(TOPIC);

    // ESP_LOGI(MQTT_DRV_TAG, "Subscribing...");
    // rc = aws_iot_mqtt_subscribe(&client, TOPIC, TOPIC_LEN, QOS0, iot_subscribe_callback_handler, NULL);
    // if(SUCCESS != rc) {
    //     ESP_LOGE(MQTT_DRV_TAG, "Error subscribing : %d ", rc);
    //     abort();
    // }

    // char cPayload[MQTT_MAX_PAYLOAD];

    // sprintf(cPayload, "%s", "hello from SDK");

    // IoT_Publish_Message_Params paramsQOS0;
    // IoT_Publish_Message_Params paramsQOS1;

    // paramsQOS0.qos = QOS0;
    // paramsQOS0.payload = (void *) cPayload;
    // paramsQOS0.isRetained = 0;

    // paramsQOS1.qos = QOS1;
    // paramsQOS1.payload = (void *) cPayload;
    // paramsQOS1.isRetained = 0;

    mqtt_bfInit();

    while(1)
    {
        static uint32_t ticks = 0;
        static uint32_t ticks_log = 0;

        if((xTaskGetTickCount() - ticks) >= pdMS_TO_TICKS(500))  // Check new message every 500ms
        {
            //Max time the yield function will wait for read messages
            rc = aws_iot_mqtt_yield(&client, 100);
            if(NETWORK_ATTEMPTING_RECONNECT == rc)
            {
                // If the client is attempting to reconnect we will skip the rest of the loop.
                continue;
            }
            ticks = xTaskGetTickCount();
        }

        if((xTaskGetTickCount() - ticks_log) >= pdMS_TO_TICKS(5000))  // display stack status every 5 sec
        {
            ESP_LOGI(MQTT_DRV_TAG, "Stack remaining for task '%s' is %lu bytes", pcTaskGetTaskName(NULL), uxTaskGetStackHighWaterMark(NULL));

            // sprintf(cPayload, "%s : %d ", "hello from ESP32 (QOS0)", i++);
            // paramsQOS0.payloadLen = strlen(cPayload);
            // rc = aws_iot_mqtt_publish(&client, TOPIC, TOPIC_LEN, &paramsQOS0);

            // sprintf(cPayload, "%s : %d ", "hello from ESP32 (QOS1)", i++);
            // paramsQOS1.payloadLen = strlen(cPayload);
            // rc = aws_iot_mqtt_publish(&client, TOPIC, TOPIC_LEN, &paramsQOS1);
            // if (rc == MQTT_REQUEST_TIMEOUT_ERROR) {
            //     ESP_LOGW(MQTT_DRV_TAG, "QOS1 publish ack not received.");
            //     rc = SUCCESS;
            // }
            ticks_log = xTaskGetTickCount();
        }
    }

    ESP_LOGE(MQTT_DRV_TAG, "An error occurred in the main loop.");
    abort();
}

int mqtt_drvSubscribe(const char *topic, const int topicLen, QoS qos, pApplicationHandler_t subscribeCallbackHanlder, void *param)
{
    ESP_LOGI(MQTT_DRV_TAG, "Subscribing to %s", topic);
    IoT_Error_t rc = FAILURE;
    rc = aws_iot_mqtt_subscribe(&client, topic, topicLen, qos, subscribeCallbackHanlder, param);

    return (int)rc;
}

int mqtt_drvPublish(const char *topic, const int topicLen, IoT_Publish_Message_Params *param)
{
    ESP_LOGI(MQTT_DRV_TAG, "Publishing to %s", topic);
    IoT_Error_t rc = FAILURE;
    rc = aws_iot_mqtt_publish(&client, topic, topicLen, param);
    
    return (int)rc;
}

void mqtt_drvInit(void)
{
    xTaskCreate(&mqtt_drvTask, "mqtt_drvTask", 9216, NULL, 5, NULL);
}
