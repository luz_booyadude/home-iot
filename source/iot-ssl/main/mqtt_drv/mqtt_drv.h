#ifndef MQTT_DRV_H
#define MQTT_DRV_H

#ifdef __cplusplus
extern "C" {
#endif

#include "aws_iot_mqtt_client_interface.h"

#define MQTT_BROKER                     "magnifyit.ddns.net"
#define MQTT_PORT                       8883
#define MQTT_CLIENT_ID                  "iot-test"
#define MQTT_MAX_PAYLOAD                100

extern const char *MQTT_DRV_TAG;

void mqtt_drvInit(void);
int mqtt_drvSubscribe(const char *topic, const int topicLen, QoS qos, pApplicationHandler_t subscribeCallbackHanlder, void *param);
int mqtt_drvPublish(const char *topic, const int topicLen, IoT_Publish_Message_Params *param);

#ifdef __cplusplus
}
#endif

#endif // MQTT_DRV_H