#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "mqtt_bf.h"
#include "mqtt_drv.h"
#include "esp_log.h"

static void mqtt_publishExample(void);

const char *MQTT_BF_TAG = "mqtt_bf";  // tag for log output

const char *topicSub = "mqtt/sub";
const char *topicPub = "mqtt/pub";

static void mqtt_subscriberHandler(AWS_IoT_Client *pClient,
                                   char *topicName,
                                   uint16_t topicNameLen,
                                   IoT_Publish_Message_Params *params,
                                   void *pData)
{
    ESP_LOGI(MQTT_BF_TAG, "Subscribe callback");
    ESP_LOGI(MQTT_BF_TAG, "%.*s\t%.*s", topicNameLen, topicName, (int) params->payloadLen, (char *)params->payload);

    mqtt_publishExample();
}

static void mqtt_publishExample(void)
{
    // send response
    static int count = 0;
    IoT_Error_t rc = FAILURE;
    IoT_Publish_Message_Params publishParam;
    char cPayload[MQTT_MAX_PAYLOAD];

    sprintf(cPayload, "Message received: %d", count++);

    publishParam.qos = QOS0;
    publishParam.payload = (void *) cPayload;
    publishParam.payloadLen = strlen(cPayload);
    publishParam.isRetained = 0;
    rc = mqtt_drvPublish(topicPub, strlen(topicPub), &publishParam);
    if(SUCCESS != rc)
    {
        ESP_LOGE(MQTT_BF_TAG, "Fail to publish message. Error code: %d", rc);
    }
}

void mqtt_bfInit(void)
{
    int errorCode = FAILURE;
    
    errorCode = mqtt_drvSubscribe(topicSub, strlen(topicSub), QOS0, &mqtt_subscriberHandler, NULL);
    if(errorCode !=  SUCCESS)
    {
        ESP_LOGE(MQTT_BF_TAG, "Fail to subscribe. Error code: %d", errorCode);
    }
}