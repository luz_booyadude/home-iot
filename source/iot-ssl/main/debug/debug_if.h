#ifndef DEBUG_IF_H
#define DEBUG_IF_H

#ifdef __cplusplus
extern "C" {
#endif

#define DEBUG_REFRESH_RATE_MS   2000 

void debugSystemInfo(void);
void debugInfoTask( void * parameter );

#ifdef __cplusplus
}
#endif

#endif // DEBUG_IF_H