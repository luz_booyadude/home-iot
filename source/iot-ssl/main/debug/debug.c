#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include "debug_if.h"

const char *TAGS = "debug";

void debugSystemInfo(void)
{
    printf("System Info:\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU cores, WiFi:%d ",
            (chip_info.model == CHIP_ESP8266) ? "ESP8266" : "UNKNOWN",
            chip_info.cores,
            chip_info.features);

    printf("silicon revision %d, ", chip_info.revision);

    // printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
    //         (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
}

void debugInfoTask(void *parameter)
{
    static uint32_t ticks = 0;
    while(1)
    {
        if((xTaskGetTickCount() - ticks) >= pdMS_TO_TICKS(DEBUG_REFRESH_RATE_MS))
        {
            ESP_LOGI(TAGS, "System heap:%u", esp_get_free_heap_size());
            ticks = xTaskGetTickCount();
        }
    }
}
