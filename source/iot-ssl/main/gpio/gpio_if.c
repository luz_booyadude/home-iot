/**
 * GPIO abstraction layer interface between ESP8266 and ESP32
 */

#include "gpio_if.h"

void gpio_pinMode(gpio_num_t gpioPin, uint8_t gpioMode)
{
    if(gpioMode == 0U)
    {
        gpio_set_direction(gpioPin, GPIO_MODE_INPUT);
    }
    else if(gpioMode == 1U)
    {
        gpio_set_direction(gpioPin, GPIO_MODE_OUTPUT);
    }
    else
    {
        gpio_set_direction(gpioPin, GPIO_MODE_DISABLE);
    }
}

uint8_t gpio_readPin(gpio_num_t gpioPin)
{
    return (uint8_t)gpio_get_level(gpioPin);
}

void gpio_writePin(gpio_num_t gpioPin, uint8_t gpioLevel)
{
    gpio_set_level(gpioPin, (uint32_t)gpioLevel);
}