/**
 * GPIO abstraction layer interface between ESP8266 and ESP32
 */

#ifndef GPIO_IF_H
#define GPIO_IF_H

#ifdef __cplusplus
extern "C" {
#endif

#include "driver/gpio.h"

typedef enum
{
    GPIO_D0 = 16U,
    GPIO_D1 = 5,
    GPIO_D2 = 4,
    GPIO_D3 = 0,
    GPIO_D4 = 2,
    GPIO_D5 = 14,
    GPIO_D6 = 12,
    GPIO_D7 = 13,
    GPIO_D8 = 15,
    GPIO_RX = 3,
    GPIO_TX = 1,
    GPIO_SD2 = 9,
    GPIO_SD3 = 10
}gpio_t;

typedef enum
{
    GPIO_INPUT = 0,
    GPIO_OUTPUT = 1,
    GPIO_DISABLE = 2
}gpioMode_t;

typedef enum
{
    GPIO_LOW = 0,
    GPIO_HIGH = 1
}gpioLevel_t;

void gpio_pinMode(gpio_num_t gpioPin, uint8_t gpioMode);
uint8_t gpio_readPin(gpio_num_t gpioPin);
void gpio_writePin(gpio_num_t gpioPin, uint8_t gpioLevel);

#ifdef __cplusplus
}
#endif

#endif // GPIO_IF_H