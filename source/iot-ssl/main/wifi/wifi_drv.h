#ifndef WIFI_H
#define WIFI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "freertos/event_groups.h"

#define WIFI_SSID "Ramli_wifi2"
#define WIFI_PASSWORD "78933476"

extern const char *TAG;
extern const int CONNECTED_BIT;
extern EventGroupHandle_t wifi_event_group;

void wifi_drvInit(void);

#ifdef __cplusplus
}
#endif

#endif // WIFI_H