private.pem.key can be any value. In this project we wont using any private key.

Make sure the server is using key from CA such as LetsEncrypt.

Get the server certificate by running:
`openssl s_client -showcerts -connect www.exampl.com:8883 </dev/null`

Copy from BEGIN CERTIFICATE up to END CERTIFICATE and paste it into `aws-root-ca.pem` and `certificate.pem.crt`