#ifndef WIFI_H
#define WIFI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "MQTTClient.h"

#define MQTT_CLIENT_THREAD_NAME         "mqtt_client_thread"
#define MQTT_CLIENT_THREAD_STACK_WORDS  4096
#define MQTT_CLIENT_THREAD_PRIO         8
#define MQTT_KEEP_ALIVE                 30
#define MQTT_BROKER                     "broker.hivemq.com"
#define MQTT_PORT                       1883
#define MQTT_VERSION                    4
#define MQTT_USERNAME                   ""
#define MQTT_PASSWORD                   ""
#define MQTT_RECV_CYCLE                 0
#define MQTT_SEND_CYCLE                 30000
#define MQTT_RECV_BUFFER                2048
#define MQTT_SEND_BUFFER                2048
#define MQTT_SECURITY                   0
#define MQTT_SESSION                    1
#define MQTT_CLIENT_ID                  "iot-test"
#define MQTT_SUB_TOPIC                  "/espressif/sub"
#define MQTT_PUB_TOPIC                  "/espressif/pub"
#define MQTT_SUB_QOS                    QOS1
#define MQTT_PUB_QOS                    QOS1
#define MQTT_PUBLISH_INTERVAL           1000
#define MQTT_PAYLOAD_BUFFER             1460

// #define MQTT_TASK

extern const char *TAGS;

void initialise_wifi(void);
void mqtt_client_thread(void *pvParameters);

#ifdef __cplusplus
}
#endif

#endif // WIFI_H