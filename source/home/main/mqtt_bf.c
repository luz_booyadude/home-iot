#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "mqtt_bf.h"
#include "esp_log.h"
#include "wifi.h"

void mqtt_parser(MQTTClient *mqttClient, size_t msgLen, char *msg)
{
    ESP_LOGI(TAGS, "Message arrived[len:%u]: %.*s", msgLen, msgLen, (char *)msg);
    
    if(msg[0] == 't')
    {
        MQTTMessage message;
        char *payload = NULL;
        payload = malloc(MQTT_PAYLOAD_BUFFER);
        int rc = 0;

        if (!payload) {
            ESP_LOGE(TAGS, "mqtt malloc err");
        } else {
            memset(payload, 0x0, MQTT_PAYLOAD_BUFFER);
        }

        message.qos = MQTT_PUB_QOS;
        message.retained = 0;
        message.payload = payload;
        sprintf(payload, "Message Received!");
        message.payloadlen = strlen(payload);

        if ((rc = MQTTPublish(mqttClient, MQTT_PUB_TOPIC, &message)) != 0) {
            ESP_LOGE(TAGS, "Return code from MQTT publish is %d", rc);
        } else {
            ESP_LOGI(TAGS, "MQTT published topic");
        }

        free(payload);
    }
}