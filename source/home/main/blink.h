#ifndef BLINK_H
#define BLINK_H

#ifdef __cplusplus
extern "C" {
#endif

void blinkTask( void * parameter );

#ifdef __cplusplus
}
#endif

#endif // BLINK_IF_H