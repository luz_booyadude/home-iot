#ifndef DEBUG_IF_H
#define DEBUG_IF_H

#ifdef __cplusplus
extern "C" {
#endif

void debugInfoTask( void * parameter );

#ifdef __cplusplus
}
#endif

#endif // DEBUG_IF_H