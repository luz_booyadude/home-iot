#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "wifi.h"
#include "mqtt_bf.h"

#define WIFI_SSID "Ramli_wifi"
#define WIFI_PASSWORD "78933476"

const char *TAGS = "example";
static MQTTClient client;

/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;

    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        break;

    case SYSTEM_EVENT_STA_DISCONNECTED:
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
        break;

    default:
        break;
    }

    return ESP_OK;
}

void initialise_wifi(void)
{
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASSWORD,
        },
    };
    ESP_LOGI(TAGS, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
}

static void messageArrived(MessageData *data)
{
    /*
    ESP_LOGI(TAGS, "Message arrived[len:%u]: %.*s", \
           data->message->payloadlen, data->message->payloadlen, (char *)data->message->payload); */
    mqtt_parser(&client, data->message->payloadlen, (char *)data->message->payload);
}

void mqtt_client_thread(void *pvParameters)
{
    char *payload = NULL;
    Network network;
    int rc = 0;
    char clientID[32] = {0};
    uint32_t count = 0;

    ESP_LOGI(TAGS, "ssid:%s passwd:%s sub:%s qos:%u pub:%s qos:%u pubinterval:%u payloadsize:%u",
             WIFI_SSID, WIFI_PASSWORD, MQTT_SUB_TOPIC,
             MQTT_SUB_QOS, MQTT_PUB_TOPIC, MQTT_PUB_QOS,
             MQTT_PUBLISH_INTERVAL, MQTT_PAYLOAD_BUFFER);

    ESP_LOGI(TAGS, "ver:%u clientID:%s keepalive:%d username:%s passwd:%s session:%d level:%u",
             MQTT_VERSION, MQTT_CLIENT_ID,
             MQTT_KEEP_ALIVE, MQTT_USERNAME, MQTT_PASSWORD,
             MQTT_SESSION, MQTT_SECURITY);

    ESP_LOGI(TAGS, "broker:%s port:%u", MQTT_BROKER, MQTT_PORT);

    ESP_LOGI(TAGS, "sendbuf:%u recvbuf:%u sendcycle:%u recvcycle:%u",
             MQTT_SEND_BUFFER, MQTT_RECV_BUFFER,
             MQTT_SEND_CYCLE, MQTT_RECV_CYCLE);

    MQTTPacket_connectData connectData = MQTTPacket_connectData_initializer;

    NetworkInit(&network);

    if (MQTTClientInit(&client, &network, 0, NULL, 0, NULL, 0) == false) {
        ESP_LOGE(TAGS, "mqtt init err");
        vTaskDelete(NULL);
    }

    payload = malloc(MQTT_PAYLOAD_BUFFER);

    if (!payload) {
        ESP_LOGE(TAGS, "mqtt malloc err");
    } else {
        memset(payload, 0x0, MQTT_PAYLOAD_BUFFER);
    }

    for (;;) {
        ESP_LOGI(TAGS, "wait wifi connect...");
        xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);

        if ((rc = NetworkConnect(&network, MQTT_BROKER, MQTT_PORT)) != 0) {
            ESP_LOGE(TAGS, "Return code from network connect is %d", rc);
            continue;
        }

        connectData.MQTTVersion = MQTT_VERSION;

        sprintf(clientID, "%s_%u", MQTT_CLIENT_ID, esp_random());

        connectData.clientID.cstring = clientID;
        connectData.keepAliveInterval = MQTT_KEEP_ALIVE;

        connectData.username.cstring = MQTT_USERNAME;
        connectData.password.cstring = MQTT_PASSWORD;

        connectData.cleansession = MQTT_SESSION;

        ESP_LOGI(TAGS, "MQTT Connecting");

        if ((rc = MQTTConnect(&client, &connectData)) != 0) {
            ESP_LOGE(TAGS, "Return code from MQTT connect is %d", rc);
            network.disconnect(&network);
            continue;
        }

        ESP_LOGI(TAGS, "MQTT Connected");

#if defined(MQTT_TASK)

        if ((rc = MQTTStartTask(&client)) != pdPASS) {
            ESP_LOGE(TAGS, "Return code from start tasks is %d", rc);
        } else {
            ESP_LOGI(TAGS, "Use MQTTStartTask");
        }

#endif

        if ((rc = MQTTSubscribe(&client, MQTT_SUB_TOPIC, MQTT_SUB_QOS, messageArrived)) != 0) {
            ESP_LOGE(TAGS, "Return code from MQTT subscribe is %d", rc);
            network.disconnect(&network);
            continue;
        }

        ESP_LOGI(TAGS, "MQTT subscribe to topic %s OK", MQTT_PUB_TOPIC);

        for (;;) {
            // MQTTMessage message;

            // message.qos = MQTT_PUB_QOS;
            // message.retained = 0;
            // message.payload = payload;
            // sprintf(payload, "message number %d", ++count);
            // message.payloadlen = strlen(payload);

            // if ((rc = MQTTPublish(&client, MQTT_PUB_TOPIC, &message)) != 0) {
            //     ESP_LOGE(TAGS, "Return code from MQTT publish is %d", rc);
            // } else {
            //     ESP_LOGI(TAGS, "MQTT published topic %s, len:%u heap:%u", MQTT_PUB_TOPIC, message.payloadlen, esp_get_free_heap_size());
            // }

            // if (rc != 0) {
            //     break;
            // }
            if ((rc = MQTTYield(&client, 10000)) != 0)

            vTaskDelay(MQTT_PUBLISH_INTERVAL / portTICK_RATE_MS);
        }

        network.disconnect(&network);
    }

    ESP_LOGW(TAGS, "mqtt_client_thread going to be deleted");
    vTaskDelete(NULL);
    return;
}