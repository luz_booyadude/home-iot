#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "gpio_if.h"
#include "blink.h"

#define BLINK_GPIO GPIO_D2

void blinkTask( void * parameter )
{
    // gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_pinMode(BLINK_GPIO, 1);
    while(1) {
        /* Blink off (output low) */
        gpio_writePin(BLINK_GPIO, 0);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        /* Blink on (output high) */
        gpio_writePin(BLINK_GPIO, 1);
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}