/**
 * This header will handle the command request from MQTT
 */

#ifndef MQTT_BF_H
#define MQTT_BF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include "MQTTClient.h"

void mqtt_parser(MQTTClient *mqttClient, size_t msgLen, char *msg);

#ifdef __cplusplus
}
#endif

#endif // MQTT_BF_H